# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 14:04:09 2017

@author: Home
"""

import pandas as pd
from matplotlib import pyplot as py
import numpy as np
def percConvert(ser):
    return ser/float(ser[-1])

def num_missing(x):
    return sum(x.isnull())

train=pd.read_csv('train.csv')
test=pd.read_csv('test.csv')
target=train['final_status']
print(train.dtypes)
#print(train.select_dtypes(['goal']).columns)
#train['goal']= np.log(train['goal'])
#train['backers_count']= np.log(train['backers_count'])
#print(train.loc[(train["disable_communication"]==True) &
# (train["final_status"]==0),["disable_communication","final_status"]])
#train.pop('final_status')
#print(train['deadline'].iloc[0])
#print(train.apply(num_missing,axis=0)) desc has 8 missing values and name has 1 value

#cd=pd.crosstab(train['final_status'],train['disable_communication'],margins=True)
#cd2=cd.apply(percConvert,axis=1)
# Continous Variables (goal, deadline, state_changed_at, created_at, launched_at, backers_count)
# 1. Univariate Analysis
#    1.1 Check for missing values: No missing values in any of the continous variables
#print(train['goal'].describe())
#    1.2 Check for outliers for continous variables
#    backers_count, goal have outliers,
#      Box plotting
#train.boxplot(column="goal")
#       Histograms
#py.hist(train['goal'], bins=15)
#py.figure()
#train['goal'].plot(kind='kde')
#train.boxplot(column="backers_count")
#train[]
#    1.3  Date variables analysis Epoch format
# Comparing Funding_amount (goal) with the target for relationship
